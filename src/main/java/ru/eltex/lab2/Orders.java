package ru.eltex.lab2;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import ru.eltex.lab7.DeleteException;

import java.io.Serializable;
import java.util.*;

@JsonAutoDetect
public class Orders<T extends Order> implements Serializable {
    public PriorityQueue<T> orders;
    public HashMap<Long,T> hashMap;
    private PriorityQueue<T> finishedOrders;
    private TreeSet<UUID> uids;

    public Orders() {
        orders = new PriorityQueue<>(timeComparator);
        uids = new TreeSet<>();
        hashMap = new HashMap<>();
    }

    public Order getOrderById(UUID id)
    {
        if (uids.contains(id))
        {
            for (Order o: orders)
            {
                if (o.uuid.equals(id))
                {
                    return o;
                }
            }
        }
        return null;
    }

    public Order removeById(UUID id) throws DeleteException
    {
        if (uids.contains(id))
        {
            for (Order o: orders)
            {
                if (o.uuid.equals(id))
                {
                    orders.remove(o);
                    return o;
                }
            }
        }
        throw new DeleteException("File not found", 2);
    }

    public boolean checkout(T order)
    {
        uids.add(order.uuid);
        hashMap.put(order.createTime, order);
        order.cart.updateIdSet();
        return orders.add(order);
    }

    public int checkOrders()
    {
        int[] finishedCount = {0};
        orders.forEach(order ->
        {
            if (order.checkStatus() == Status.FINISHED)
            {
                finishedCount[0]++;
            }
        });
        return finishedCount[0];
    }

    public PriorityQueue<T> removeFinished()
    {
        /*
        удаление через итератор
        Iterator it = orders.iterator();
        while (it.hasNext())
        {
            Order o = (Order)it.next();
            if (o.status == Status.FINISHED)
            {
                it.remove();
            }
        }*/
        PriorityQueue<T> removeCollection = new PriorityQueue<>(timeComparator);
        orders.forEach(order -> {
            if (order.status == Status.FINISHED)
            {
                uids.remove(order.uuid);
                removeCollection.add(order);
                hashMap.remove(order.createTime, order);
            }
        });
        if (orders.removeAll(removeCollection))
        {
            return removeCollection;
        }
        else
        {
            return null;
        }
    }

    public void showAll()
    {
        StringBuilder sb = new StringBuilder();
        int i = 1;
        for (T elem : orders) {
            sb.append(i + ") " + elem.cred.uuid
                    + "\t" + elem.status
                    + "\t" + elem.createTime
                    + "\t" + elem.waitTime
                    + "\n" + "Cart:" + "\n");
            sb.append(elem.cart.showAll());
            i++;

        }
        System.out.print(sb.toString());
    }

    public Comparator<T> timeComparator = new Comparator<T>()
    {
        @Override
        public int compare(T c1, T c2) {
            return (int) (c1.createTime - c2.createTime);
        }
    };
}
