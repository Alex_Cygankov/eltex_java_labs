package ru.eltex.lab2;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "credentials")
@JsonAutoDetect
public class Credentials implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "uuid", unique = true)
    @Type(type = "uuid-char")
    public UUID uuid;

    @NotNull
    @Size(max = 100)
    @Column(name = "name")
    public String name;

    @NotNull
    @Size(max = 100)
    @Column(name = "secondName")
    public String secondName;

    @NotNull
    @Size(max = 100)
    @Column(name = "thirdName")
    public String thirdName;

    @NotNull
    @Size(max = 100)
    @Column(name = "email")
    public String email;

    public Credentials(){}

    public Credentials(String name, String secondName, String thirdName, String email) {
        uuid = UUID.randomUUID();
        this.name = name;
        this.secondName = secondName;
        this.thirdName = thirdName;
        this.email = email;
    }
}
