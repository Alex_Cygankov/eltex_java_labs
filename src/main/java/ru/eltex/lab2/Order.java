package ru.eltex.lab2;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "orders")
@JsonAutoDetect
@JsonIgnoreProperties(
        value = {"hibernateLazyInitializer", "handler"},
        allowGetters = true)
public class Order implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "uuid", unique = true)
    @Type(type = "uuid-char")
    public UUID uuid;

    @Autowired
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinTable(name = "order_shopingCart",
            joinColumns = { @JoinColumn(name = "order_id") },
            inverseJoinColumns = { @JoinColumn(name = "shopingCart_id") })
    public ShoppingCart cart;

    @Autowired
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinColumn(name = "credential_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    public Credentials cred;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    public Status status;

    @Column(name = "createTime")
    public long createTime;

    @Column(name = "waitTime")
    public long waitTime;// in ms

    @Column(name = "clientPortUDP")
    public int clientPortUDP;


    public Order()
    {    }

    public Order(ShoppingCart cart, Credentials cred, long waitTime)
    {
        uuid = UUID.randomUUID();
        this.cred = cred;
        this.cart = cart;
        status = Status.WAITING;
        createTime = new Date().getTime();
        this.waitTime = waitTime;
    }

    public Status checkStatus()
    {
        return status = new Date().getTime() < createTime + waitTime ? Status.WAITING : Status.FINISHED;
    }
}
