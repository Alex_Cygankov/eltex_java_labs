package ru.eltex.lab2;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GeneratorType;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.beans.factory.annotation.Autowired;
import ru.eltex.lab1.Drink;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;
import java.util.UUID;

@Entity
@Table(name = "shopingCarts")
@JsonAutoDetect
@JsonIgnoreProperties(
        value = {"idSet", "hibernateLazyInitializer", "handler"},
        allowGetters = true)
public class ShoppingCart<T extends Drink> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE},
            fetch = FetchType.LAZY,
            targetEntity=Drink.class)
    @JoinTable(name = "drink_carts",
            joinColumns = { @JoinColumn(name = "drink_id") },
            inverseJoinColumns = { @JoinColumn(name = "cart_id") })
    public List<T> cart;

    @Column(insertable = false, updatable = false)
    private TreeSet<UUID> idSet;
    public int size(){ return idSet.size();}


    public ShoppingCart()
    {
        cart = new LinkedList<T>();
        idSet = new TreeSet<>();
    }

    public ShoppingCart(LinkedList<T> cart)
    {
        this.cart = cart;
        idSet = new TreeSet<>();
        for (Drink drink: cart)
        {
            idSet.add(drink.objectId);
        }
    }

    public boolean add(T item)
    {
        idSet.add(item.objectId);
        return cart.add(item);
    }

    public T delete(int index)
    {
        idSet.remove(cart.get(index).objectId);
        return cart.remove(index);
    }

    public boolean delete(Object o)
    {
        return cart.remove(o);
    }

    public boolean find(UUID id)
    {
        return idSet.contains(id);
    }

    public void updateIdSet()
    {
        idSet = new TreeSet<UUID>();
        for (Drink drink: cart)
        {
            idSet.add(drink.objectId);
        }
    }

    public String showAll()
    {
        StringBuilder sb = new StringBuilder();
        int i = 1;
        for (T elem : cart) {
            sb.append(i + ") " + elem.objectId
                    + "\t" + elem.name
                    + "\t" + elem.price
                    + "\t" + elem.firmName
                    + "\t" + elem.country + "\n");
            i++;
        }
        return sb.toString();
    }
}
