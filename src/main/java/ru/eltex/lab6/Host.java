package ru.eltex.lab6;

import ru.eltex.lab2.Orders;

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.*;

public class Host {

    private int serverPortUDP;
    private int[] clientPortsUDP;
    private int portTCP;
    private ServerSocket serverTCPSocket = null;
    private DatagramSocket serverUDPSocket = null;
    private Properties idToPort = new Properties();
    Socket clientSocket = null;
    Thread portTCPSender = null;
    volatile boolean sendingTCP = false;
    Orders orders;

    public Host(int portTCP, Orders orders) {
        this.portTCP = portTCP;
        this.orders = orders;
        clientPortsUDP = new int[]{8033, 8034, 8035};
        serverPortUDP = 8050;
        if (serverUDPSocket == null) {
            try {
                serverUDPSocket = new DatagramSocket(serverPortUDP);
            } catch (SocketException e) {
                e.printStackTrace();
            }
        }
        createSocket();
        Thread notificator = new Thread(new Notificator(3000, orders, idToPort, serverUDPSocket));
        notificator.start();
    }

    private void createSocket()
    {
        while (serverTCPSocket == null)
        {
            try {
                serverTCPSocket = new ServerSocket(portTCP);
            }
            catch (IOException e) {
                System.out.println("Порт занят: " + portTCP);
                portTCP++;
            }
        }
    }

    public void sendPortTCP()
    {
        portTCPSender = new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while (sendingTCP)
                {
                    try {
                        byte[] data;
                        data = ByteBuffer.allocate(Integer.BYTES).putInt(portTCP).array();
                        InetAddress addr = InetAddress.getLocalHost();

                        DatagramPacket pac;

                        //создание пакета данных

                        pac = new DatagramPacket(data,  Integer.BYTES, addr, clientPortsUDP[i%clientPortsUDP.length]);

                        serverUDPSocket.send(pac);//отправление пакета

                        //System.out.println("TCP порт отправлен на UDP порт " + clientPortsUDP[i%clientPortsUDP.length]);
                        i++;
                    }
                    catch (
                            IOException e) {

                        // неверный адрес получателя

                        e.printStackTrace();

                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        sendingTCP = true;
        portTCPSender.start();
    }

    public void listenTCP()
    {
        while (true) {
            try {
                clientSocket = serverTCPSocket.accept();
                if (clientSocket != null) {
                    Thread orderReciver = new Thread(new ServerConnectionProcessor(clientSocket, orders, serverTCPSocket, idToPort));
                    orderReciver.start();
                }
            } catch (IOException e) {
                System.out.println("Ошибка при подключении к порту: " + portTCP);
                System.exit(-1);
            }
        }
    }

//    public Order reciveOrder()
//    {
//        Order in = null;
//        try (DataInputStream dis = new DataInputStream(clientSocket.getInputStream());
//             ObjectInputStream ois = new ObjectInputStream(dis) )
//        {
//
//            try {
//                if (((in = (Order) ois.readObject()) != null)) {
//                    byte[] data;
//                    orders.checkout(in);
//                    idToPort.setProperty(in.uuid.toString(), Integer.toString(clientSocket.getPort()));
//                    Thread.sleep(in.createTime - new Date().getTime() + in.waitTime);
//                    orders.checkOrders();
//
//                    data = ByteBuffer.allocate(4).putInt(Status.FINISHED.ordinal()).array();
//                    InetAddress addr = InetAddress.getLocalHost();
//                    DatagramPacket pac;
//
//                    //создание пакета данных
//
//                    pac = new DatagramPacket(data, Integer.BYTES, addr, portUDP);
//
//                    serverUDPSocket.send(pac);//отправление пакета
//
//                    System.out.println("Заказ обработан");
//                }
//            } catch (ClassNotFoundException e) {
//                e.printStackTrace();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//             catch(IOException e)
//            {
//                e.printStackTrace();
//            }
//        } catch (IOException e) {
//            System.out.println("Не удалось получить поток ввода.");
//            System.exit(-1);
//        }
//        return in;
//    }
}
