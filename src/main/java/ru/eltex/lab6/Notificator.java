package ru.eltex.lab6;

import ru.eltex.lab2.Order;
import ru.eltex.lab2.Orders;
import ru.eltex.lab2.Status;
import ru.eltex.lab4.ACheck;

import java.io.IOException;
import java.net.*;
import java.nio.*;
import java.util.*;

public class Notificator extends ACheck {

    private Properties idToPort = new Properties();
    private DatagramSocket serverUDPSocket = null;


    public Notificator(long interval, Orders orders, Properties idToPort, DatagramSocket serverUDPSocket)
    {
        this.interval = interval;
        this.orders = orders;
        this.idToPort = idToPort;
        this.serverUDPSocket = serverUDPSocket;
    }
    @Override
    public void run() {
        while (runable)
        {
            PriorityQueue<Order> finished = null;
            int finishedOrders;
            synchronized (orders) {
                finishedOrders = orders.checkOrders();
            }
            if ( finishedOrders > 0){
                synchronized (orders) {
                    finished = orders.removeFinished();
                }
                for (Order o : finished) {
                    String s = idToPort.getProperty(o.uuid.toString());
                    synchronized (idToPort) {
                        idToPort.remove(o.uuid.toString());
                    }
                    if (s != null) {
                        int clientPort = Integer.parseInt(s);
                        byte[] data;
                        data = ByteBuffer.allocate(4).putInt(Status.FINISHED.ordinal()).array();
                        InetAddress addr = null;
                        try {
                            addr = InetAddress.getLocalHost();
                        } catch (UnknownHostException e) {
                            e.printStackTrace();
                        }
                        DatagramPacket pac;

                        pac = new DatagramPacket(data, Integer.BYTES, addr, clientPort);
                        try {
                            serverUDPSocket.send(pac);//отправление пакета
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        System.out.println("Заказ обработан " + clientPort);
                    }
                }
            }
            try
            {
                Thread.sleep(interval);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }
}
