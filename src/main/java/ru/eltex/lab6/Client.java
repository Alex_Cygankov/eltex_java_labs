package ru.eltex.lab6;

import ru.eltex.lab2.Order;
import ru.eltex.lab2.Status;
import ru.eltex.helpers.DataGeneratorHelper;

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.time.LocalDateTime;

public class Client {
    private int portUDP = 8033;
    private int portTCP = -1;
    private InetAddress addrHost = null;
    private InetAddress addrClient = null;
    private DatagramSocket clientUDPSocket = null;
    public volatile boolean isListening = false;
    public volatile boolean isSpaming = false;

    public Client() throws UnknownHostException {
        addrClient = InetAddress.getLocalHost();
    }

    public Client(int portUDP, InetAddress address)
    {
        this.portUDP = portUDP;
        try {
            addrClient = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public void getPortTCP()
    {
        while (portTCP == -1) {
            System.out.println("Порт UDP: " + portUDP);
            System.out.println("Прием данных…");

            try { // прием порта
                byte data[] = new byte[Integer.BYTES];
                DatagramPacket pac = new DatagramPacket(data, data.length);

                if (clientUDPSocket == null) {
                    clientUDPSocket = new DatagramSocket(portUDP, addrClient);
                }

                try {

                /* установка времени ожидания: если в течение 180 секунд
                не принято ни одного пакета, прием данных заканчивается*/

                    clientUDPSocket.setSoTimeout(180000);
                    while (portTCP == -1) {
                        clientUDPSocket.receive(pac);
                        portTCP = ByteBuffer.wrap(data).getInt();
                        addrHost = pac.getAddress();
                        System.out.println("Порт получен: " + portTCP);
                    }

                } catch (SocketTimeoutException e) {
                    // если время ожидания вышло
                    System.out.println("Истекло время ожидания, прием данных закончен");

                } catch (SocketException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            } catch (IOException e) {

                e.printStackTrace();

            }
            try
            {
                Thread.sleep(1000);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }

    public void sendOrderTCP()
    {
        Thread orderSender = new Thread(new Runnable(){
            @Override
            public void run() {
                while (isSpaming)
                {
                    Order order = DataGeneratorHelper.generateOrder(10000);
                    try  {
                        System.out.println("Ascking TCP socket . . .");
                        //  запрос клиента на соединение
                        Socket sock = new Socket(addrHost, portTCP);
                        System.out.println("Socket TCP recived . . .");
                        // Исходящий поток

                        // Отправляем запрос и аргументы
                        try (DataOutputStream outStream = new DataOutputStream(sock.getOutputStream());
                             ObjectOutputStream oos = new ObjectOutputStream(outStream))
                        {
                            order.clientPortUDP = portUDP;
                            oos.writeObject(order);
                            System.out.println("Order sent . . .");
                        }
                        catch (FileNotFoundException e)
                        {
                            e.printStackTrace();
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                        finally {
                            // Завершаем потоки и закрываем сокет
                            sock.close();

                        }
                    }
                    catch(Exception e)    {  System.err.println(e);     }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        isSpaming = true;
        orderSender.start();
    }

    public void listenUDP()
    {
        Thread listener = new Thread(new Runnable() {
            @Override
            public void run() {
                while (isListening)
                {
                    System.out.println("Прием данных…");

                    try {
                        byte data[] = new byte[Integer.BYTES];
                        DatagramPacket pac = new DatagramPacket(data, data.length);
                        ByteArrayOutputStream os = new ByteArrayOutputStream(portTCP);

                        try {
                /* установка времени ожидания: если в течение 600 секунд
                не принято ни одного пакета, прием данных заканчивается*/
                            boolean recived = false;
                            clientUDPSocket.setSoTimeout(600000);
                            while (!recived) {
                                clientUDPSocket.receive(pac);
                                os.write(data);
                                os.flush();
                                int val = ByteBuffer.wrap(data).getInt();
                                if (val == Status.FINISHED.ordinal())
                                {
                                    recived = true;
                                    System.out.println(LocalDateTime.now().toString() + " Заказ обработан.");
                                }
                                else if (val == Status.WAITING.ordinal())
                                {
                                    recived = true;
                                    System.out.println(LocalDateTime.now().toString() + " Заказ в обработке.");
                                }
                                else
                                {
                                }
                            }

                        } catch (SocketTimeoutException e) {
                            // если время ожидания вышло
                            os.close();
                            System.out.println("Истекло время ожидания, прием данных закончен");

                        } catch (SocketException e1) {
                            e1.printStackTrace();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }

                    } catch (IOException e) {

                        e.printStackTrace();

                    }

                }
            }
        });
        isListening = true;
        listener.start();
    }
}
