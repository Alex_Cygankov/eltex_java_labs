package ru.eltex.lab6;

import ru.eltex.lab2.Order;
import ru.eltex.lab2.Orders;
import ru.eltex.lab4.ACheck;

import java.io.*;
import java.net.*;
import java.util.*;

public class ServerConnectionProcessor extends ACheck
{
    private Socket clientSocket;
    private Orders orders;
    private ServerSocket serverTCPSocket;
    private Properties idToPort;


    public ServerConnectionProcessor(Socket s, Orders orders, ServerSocket serverTCPSocket, Properties idToPort)
    {
        clientSocket = s;
        this.orders = orders;
        this.serverTCPSocket = serverTCPSocket;
        this.idToPort = idToPort;
    }

    @Override
    public void run()
    {
        try (DataInputStream dis = new DataInputStream(clientSocket.getInputStream());
             ObjectInputStream ois = new ObjectInputStream(dis) )
        {

            Order in = null;
            try {
                if (((in = (Order) ois.readObject()) != null)) {
                    synchronized (idToPort){
                        idToPort.setProperty(in.uuid.toString(), Integer.toString(in.clientPortUDP));
                    }
                    synchronized (orders) {
                        orders.checkout(in);
                    }
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        } catch (IOException e) {
            System.out.println("Не удалось получить поток ввода.");
            System.exit(-1);
        }
    }
}