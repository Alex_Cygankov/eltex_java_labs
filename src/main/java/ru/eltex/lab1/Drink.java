package ru.eltex.lab1;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import ru.eltex.lab2.ShoppingCart;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.*;


@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
//@MappedSuperclass
//@EntityListeners(AuditingEntityListener.class)
@JsonAutoDetect
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property="type")
@JsonSubTypes({
        @JsonSubTypes.Type(value=Tea.class, name="tea"),
        @JsonSubTypes.Type(value=Coffee.class, name="coffee")
})
@JsonIgnoreProperties(
        value = {"count", "rnd", "carts", "hibernateLazyInitializer", "handler"},
        allowGetters = true
)
public abstract class Drink implements ICrudAction, Serializable
{
    @Column(insertable = false, updatable = false)
    protected static int count = 0;

    @Column(insertable = false, updatable = false)
    protected static Random rnd = new Random();

//    @ManyToMany(fetch = FetchType.LAZY,
//            cascade = {
//                    CascadeType.PERSIST,
//                    CascadeType.MERGE
//            },
//            mappedBy = "cart")
//    private Set<ShoppingCart> carts;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @NotNull
    @Column(name = "objectId", unique = true)
    @Type(type = "uuid-char")
    public UUID objectId = UUID.randomUUID();

    @NotNull
    @Size(max = 100)
    @Column(name = "name")
    public String name;

    @Column(name = "price")
    public double price;

    @NotNull
    @Size(max = 100)
    @Column(name = "firmName")
    public String firmName;

    @NotNull
    @Size(max = 100)
    @Column(name = "country")
    public String country;

    public static int getCount() { return count;}

//    public Set<ShoppingCart> getCarts()
//    {
//        if (carts == null)
//        {
//            carts = new HashSet<>();
//        }
//        return carts;
//    }
//
//    public void setCarts(Set<ShoppingCart> carts)
//    {
//        this.carts = carts;
//    }
}
