package ru.eltex.lab1;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.web.bind.annotation.GetMapping;
import ru.eltex.lab2.ShoppingCart;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Scanner;
import java.util.UUID;

@Entity
@Table(name = "tea")
@JsonAutoDetect
public class Tea extends Drink
{
    @Column(name = "typePackage")
    @Enumerated(EnumType.STRING)
    public TypePackage typePackage;

    public Tea()
    {
        name = "";
        price = 0;
        firmName = "";
        country = "";
    }

    public Tea(String name, double price, String firmName, String country, TypePackage typePackage, ShoppingCart cart)
    {
        objectId = UUID.randomUUID();
        this.name = name;
        this.price = price;
        this.firmName = firmName;
        this.country = country;
        this.typePackage = typePackage;
        count++;
    }

    @Override
    public void create()
    {
        objectId = UUID.randomUUID();
        this.name = "";
        this.firmName = "";
        this.country = "";
        for (int i = 0; i < 10; i++)
        {
            this.name += (char)('a' + rnd.nextInt(26));
            this.firmName += (char)('a' + rnd.nextInt(26));
            this.country += (char)('a' + rnd.nextInt(26));
        }
        this.price = rnd.nextDouble();
        this.typePackage = TypePackage.values()[rnd.nextInt(2)];
        count++;
    }

    @Override
    public void read()
    {
        System.out.println("Id:\t" + objectId.toString()
                + "\nName:\t" + name
                + "\nType:\t" + typePackage.name()
                + "\nPrice:\t" + price
                + "\nFirm Name:\t" + firmName
                + "\nCountry:\t" + country);
    }

    @Override
    public void update()
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Set new name (\"\" to leave old): ");
        String name = in.nextLine();
        this.name = name == "" ? this.name : name;

        System.out.println("Set new coffee type (\"\" to leave old): ");
        for (TypePackage type: TypePackage.values()) {
            System.out.printf("%d)%s", type.ordinal(), type.name());
        }
        if (in.hasNextInt()) {
            int typeIn = in.nextInt();
            if (typeIn >= 0 && typeIn < TypePackage.values().length) {
                typePackage = TypePackage.values()[typeIn];
            }
        }

        System.out.println("Set new price (<=0 to leave old): ");
        double price = in.nextDouble();
        this.price = price <= 0 ? this.price : price;
        System.out.println("Enter new firmName (\"\" to leave old): ");
        String firmName = in.nextLine();
        this.firmName = firmName == "" ? this.firmName : firmName;
        System.out.println("Enter new country (\"\" to leave old): ");
        String country = in.nextLine();
        this.country = country == "" ? this.country : country;
        in.close();
    }

    @Override
    public void delete()
    {
        if (this.objectId != null)
        {
            this.objectId = null;
            this.name = null;
            this.firmName = null;
            this.price = -1;
            this.country = null;
            count--;
        }
    }
}
