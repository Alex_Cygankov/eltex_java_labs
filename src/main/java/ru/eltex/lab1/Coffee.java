package ru.eltex.lab1;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import ru.eltex.lab2.ShoppingCart;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;
import java.util.Scanner;

@Entity
@Table(name = "coffee")
@JsonAutoDetect
public class Coffee extends Drink
{
    @Column(name = "typeCoffee")
    @Enumerated(EnumType.STRING)
    public TypeCoffee typeCoffee;

    public Coffee()
    {
        name = "";
        price = 0;
        firmName = "";
        country = "";
        objectId = null;
    }

    public Coffee(String name, double price, String firmName, String country, TypeCoffee typeCoffee, ShoppingCart cart)
    {
        objectId = UUID.randomUUID();
        this.name = name;
        this.price = price;
        this.firmName = firmName;
        this.country = country;
        this.typeCoffee = typeCoffee;
        count++;
    }


    @Override
    public void create()
    {
        if (objectId == null)
        {
            objectId = UUID.randomUUID();
            this.name = "";
            this.firmName = "";
            this.country = "";
            for (int i = 0; i < 10; i++)
            {
                this.name += (char)('a' + rnd.nextInt(26));
                this.firmName += (char)('a' + rnd.nextInt(26));
                this.country += (char)('a' + rnd.nextInt(26));
            }
            this.price = rnd.nextDouble();
            this.typeCoffee = TypeCoffee.values()[rnd.nextInt(2)];
            count++;
        }
    }

    @Override
    public void read()
    {
        if (objectId != null)
            System.out.println("Id:\t" + objectId.toString()
                + "\nName:\t" + name
                + "\nType:\t" + typeCoffee.name()
                + "\nPrice:\t" + price
                + "\nFirm Name:\t" + firmName
                + "\nCountry:\t" + country);
        else
            System.out.println("void record");
    }

    @Override
    public void update()
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Set new name (\"\" to leave old): ");
        String name = in.nextLine();
        this.name = name == "" ? this.name : name;

        System.out.println("Set new coffee type (\"\" to leave old): ");
        for (TypeCoffee type: TypeCoffee.values()) {
            System.out.printf("%d)%s", type.ordinal(), type.name());
        }
        if (in.hasNextInt()) {
            int typeIn = in.nextInt();
            if (typeIn >= 0 && typeIn < TypeCoffee.values().length) {
                typeCoffee = TypeCoffee.values()[typeIn];
            }
        }

        System.out.println("Set new price (<=0 to leave old): ");
        double price = in.nextDouble();
        this.price = price <= 0 ? this.price : price;

        System.out.println("Enter new firmName (\"\" to leave old): ");
        String firmName = in.nextLine();
        this.firmName = firmName == "" ? this.firmName : firmName;

        System.out.println("Enter new country (\"\" to leave old): ");
        String country = in.nextLine();
        this.country = country == "" ? this.country : country;
        in.close();
    }

    @Override
    public void delete()
    {
        if (this.objectId != null)
        {
            this.objectId = null;
            this.name = null;
            this.firmName = null;
            this.price = -1;
            this.country = null;
            count--;
        }
    }
}

