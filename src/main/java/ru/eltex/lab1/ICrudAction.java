package ru.eltex.lab1;

interface ICrudAction
{
    public void create();
    public void read();
    public void update();
    public void delete();
}
