package ru.eltex.lab7;

public class DeleteException extends Exception
{
    //1 - нет такого заказа, код 2 - файл поврежден, 3 - неправильная команда
    private int number;
    public int getNumber(){return number;}
    public DeleteException(String message, int num){
        super(message);
        number = num;
    }
}
