package ru.eltex.lab7;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.*;
import ru.eltex.helpers.DataGeneratorHelper;
import ru.eltex.lab1.Drink;
import ru.eltex.lab2.Order;
import ru.eltex.lab2.Orders;
import ru.eltex.lab5.ManagerOrderJSON;

import java.io.*;
import java.util.UUID;


@RestController
public class ServiceController
{
    private String command;
    private String id;
    private Command modelCommand;
    private Orders orders;
    private DataGeneratorHelper dataGenerator;
    public static Logger logger = LoggerFactory.getLogger(Application.class);

    public Orders getOrders()
    {
        if (dataGenerator == null)
        {
            dataGenerator = new DataGeneratorHelper();

        }
        if (orders == null)
        {
            orders = dataGenerator.generateOrders();
        }
        return orders;
    }




    @GetMapping("/index")
    Object index()
    {
        return "hello";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Object requestReadAll(@RequestParam(name="command", required=true) String command,
                                 @RequestParam(name="order_id", required=false, defaultValue = "") String order_id,
                                 @RequestParam(name="card_id", required=false, defaultValue = "") String card_id) {

        logger.info("command=" + command + "order_id" + order_id + "card_id" + card_id);
        if (modelCommand == null)
        {
            modelCommand = new Command();
        }
        this.id = "no_id";
        modelCommand.setCommand(command);
        modelCommand.setId(this.id);
        String fileName = "savedAll.json";
        switch (command)
        {
            case "readall":{
                ManagerOrderJSON moJSON = new ManagerOrderJSON();
                Orders orders = new Orders();
                try
                {
                    for (Order o : moJSON.readAll(fileName) )
                    {
                        orders.checkout(o);
                    }
                    return orders;
                } catch (DeleteException e)
                {
                    e.printStackTrace();
                }
            }
            case "readById":{
                try (FileReader fr = new FileReader(fileName);
                     BufferedReader br = new BufferedReader(fr))
                {
                    UUID uuid = UUID.fromString(order_id);
                    ObjectMapper mapper = new ObjectMapper();
                    String data;
                    while ((data = br.readLine()) != null)
                    {
                        Order order = mapper.readValue(data, Order.class);
                        if (order.uuid.equals(uuid))
                        {
                            return data;
                        }
                    }
                }catch (Exception e) {
                    try
                    {
                        logger.error( "Error 2:\n" + e.getMessage());
                        throw new DeleteException(e.getMessage(), 2);
                    } catch (DeleteException ex)
                    {
                        return "2";
                    }
                };
            }
            case "addToCard":{
                ManagerOrderJSON moJSON = new ManagerOrderJSON();
                Orders orders = new Orders();
                try
                {
                    for (Order o : moJSON.readAll(fileName) )
                    {
                        orders.checkout(o);
                    }
                } catch (DeleteException e)
                {
                    e.printStackTrace();
                }
                Drink newDrink = DataGeneratorHelper.generateDrink(orders.getOrderById(UUID.fromString(card_id)).cart);
                moJSON.saveAll(orders, fileName);
                return newDrink.objectId.toString();
            }
            case "delById":{
                try
                {
                    ManagerOrderJSON moJSON = new ManagerOrderJSON();
                    Orders orders = new Orders();
                    for (Order o : moJSON.readAll(fileName) )
                    {
                        orders.checkout(o);
                    }
                    orders.removeById(UUID.fromString(order_id));
                    moJSON.saveAll(orders, fileName);
                    return "0";
                }
                catch (DeleteException e)
                {
                    logger.error( "Error " + e.getNumber() + ":\n" + e.getMessage());
                    return Integer.toString(e.getNumber());
                }
            }
            default:{
                try
                {
                    throw new DeleteException("Неправильная команда", 3);
                } catch (DeleteException e)
                {
                    logger.error( "Error " + e.getNumber() + ":\n" + e.getMessage());
                    return Integer.toString(e.getNumber());
                }
            }
        }
    }
}
