package ru.eltex.lab8.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.eltex.lab2.Credentials;
import ru.eltex.lab2.Order;

@Repository
public interface CredentialsRepository extends JpaRepository<Credentials,Long>
{
}
