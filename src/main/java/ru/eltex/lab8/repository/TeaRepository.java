package ru.eltex.lab8.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.eltex.lab1.Tea;
import ru.eltex.lab2.Order;

@Repository
public interface TeaRepository extends JpaRepository<Tea,Long>
{
}
