package ru.eltex.lab8.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.eltex.lab2.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order,Long>
{
}
