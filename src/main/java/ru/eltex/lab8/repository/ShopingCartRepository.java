package ru.eltex.lab8.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.eltex.lab2.Order;
import ru.eltex.lab2.ShoppingCart;

@Repository
public interface ShopingCartRepository extends JpaRepository<ShoppingCart,Long>
{
}
