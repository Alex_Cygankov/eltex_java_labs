package ru.eltex.lab8.model;

public class Result
{
    private String command;
    private String content;

    public Result (String command, String content)
    {
        this.command = command;
        this.content = content;
    }

    public String getCommand()
    {
        return command;
    }

    public void setCommand(String command)
    {
        command = command;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        content = content;
    }
}
