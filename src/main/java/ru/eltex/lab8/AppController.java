package ru.eltex.lab8;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import ru.eltex.helpers.DataGeneratorHelper;
import ru.eltex.lab1.Drink;
import ru.eltex.lab2.Order;
import ru.eltex.lab2.Orders;
import ru.eltex.lab5.ManagerOrderJSON;
import ru.eltex.lab7.Application;
import ru.eltex.lab7.Command;
import ru.eltex.lab7.DeleteException;
import ru.eltex.lab8.model.Result;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.eltex.lab8.repository.OrderRepository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.UUID;

@RestController
public class AppController
{

    @Autowired
    private OrderRepository orderRepository;

    private String id;
    private Command modelCommand;
    public static Logger logger = LoggerFactory.getLogger(Application.class);
    private Orders orders;
    private DataGeneratorHelper dataGeneratorHelper = new DataGeneratorHelper();


    @GetMapping(value = "/index")
    public String index(Model model)
    {
        return "index";
    }

    @RequestMapping(value = "/result",method = RequestMethod.GET)
    public String result(@RequestParam(name = "command", required = true) String command,
                         @RequestParam(name = "argument", required = false) String argument,
                         Model model)
    {
        model.addAttribute(new Result(command, argument + Math.PI));
        return "result";
    }

    @GetMapping(value = "/fillTable")
    public String fillTable(Model model)
    {
        orderRepository.deleteAllInBatch();

		DataGeneratorHelper dataGeneratorHelper = new DataGeneratorHelper();

		Orders orders = dataGeneratorHelper.generateOrders();

		orderRepository.saveAll(orders.orders);
        return "filled";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Object requestReadAll(@RequestParam(name="command", required=true) String command,
                                 @RequestParam(name="order_id", required=false, defaultValue = "") String order_id,
                                 @RequestParam(name="card_id", required=false, defaultValue = "") String card_id) {

        logger.info("command=" + command + "order_id" + order_id + "card_id" + card_id);
        if (modelCommand == null)
        {
            modelCommand = new Command();
        }
        this.id = "no_id";
        modelCommand.setCommand(command);
        modelCommand.setId(this.id);
        switch (command)
        {
            case "readall":{
                ManagerOrderJSON moJSON = new ManagerOrderJSON();
                Orders orders = new Orders();
                for (Order o : orderRepository.findAll())
                {
                    orders.checkout(o);
                }
                return orders;
            }
            case "readById":{
                return  orderRepository.findById((long) Integer.parseInt(order_id));
            }
            case "addToCard":{
                Order order = orderRepository.findById((long) Integer.parseInt(card_id)).get();
                order.cart.updateIdSet();
                Drink drink = DataGeneratorHelper.generateDrink(order.cart);
                orderRepository.save(order);
                return drink.objectId.toString();
            }
            case "delById":{
                Order order = orderRepository.findById((long) Integer.parseInt(order_id)).get();
                orderRepository.delete(order);
                return 0;
            }
            default:{
                try
                {
                    throw new DeleteException("Неправильная команда", 3);
                } catch (DeleteException e)
                {
                    logger.error( "Error " + e.getNumber() + ":\n" + e.getMessage());
                    return Integer.toString(e.getNumber());
                }
            }
        }
    }
}
