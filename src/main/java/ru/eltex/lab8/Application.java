package ru.eltex.lab8;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import ru.eltex.helpers.DataGeneratorHelper;
import ru.eltex.lab2.Orders;
import ru.eltex.lab8.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableJpaAuditing
@EntityScan({"ru.eltex.lab2", "ru.eltex.lab1"})
public class Application //implements CommandLineRunner
{
//	@Autowired
//	private OrderRepository orderRepository;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

//	@Override
//	public void run(String... args) throws Exception {
//		orderRepository.deleteAllInBatch();
//
//		DataGeneratorHelper dataGeneratorHelper = new DataGeneratorHelper();
//
//		Orders orders = dataGeneratorHelper.generateOrders();
//
//		orderRepository.saveAll(orders.orders);
//	}

}
