package ru.eltex.lab4;

import ru.eltex.lab2.Orders;
import ru.eltex.helpers.DataGeneratorHelper;

public class OrderGenerator extends ACheck {
    private final Object lock = new Object();

    public OrderGenerator(long interval, Orders orders)
    {
        this.interval = interval;
        this.orders = orders;
    }

    @Override
    public void run()
    {
        while (runable)
        {
            synchronized (orders)
            {
                if (orders.checkout(DataGeneratorHelper.generateOrder(10000)))
                {
                    System.out.println("Order added");
                }
            }
            try
            {
                Thread.sleep(interval);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }

    }
}
