package ru.eltex.lab4;

import ru.eltex.lab2.Orders;

public class CheckerWaitings extends ACheck {

    public CheckerWaitings(long interval, Orders orders)
    {
        this.interval = interval;
        this.orders = orders;
    }

    @Override
    public void run()
    {
        while (runable)
        {
            synchronized (orders)
            {
                int finished = orders.checkOrders();
                System.out.println(finished + " orders finished.");
            }
            try
            {
                Thread.sleep(interval);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }
}
