package ru.eltex.lab4;

import ru.eltex.lab1.Drink;
import ru.eltex.lab2.Orders;

import java.util.PriorityQueue;

public class CheckerFinished extends ACheck
{
    private final Object lock = new Object();

    public CheckerFinished(long interval, Orders orders)
    {
        this.interval = interval;
        this.orders = orders;
    }

    @Override
    public void run()
    {
        while (runable)
        {
            synchronized (orders) {
                PriorityQueue<Drink> deleted = orders.removeFinished();
                if (deleted != null) {
                    System.out.println(deleted.size() + " orders deleted.");
                }
            }
            try
            {
                Thread t = new Thread();

                Thread.sleep(interval);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }
}
