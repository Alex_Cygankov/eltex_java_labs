package ru.eltex.lab5;

import ru.eltex.lab2.Order;
import ru.eltex.lab2.Orders;
import ru.eltex.lab7.DeleteException;

import java.util.PriorityQueue;
import java.util.UUID;

public interface IOrder {
    Order readById(UUID id, String fileName);
    void saveById(UUID id, Orders orders, String fileName);
    PriorityQueue<Order> readAll(String fileName) throws DeleteException;
    void saveAll(Orders orders, String fileName);
}
