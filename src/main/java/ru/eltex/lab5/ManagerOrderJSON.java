package ru.eltex.lab5;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.eltex.lab2.Order;
import ru.eltex.lab2.Orders;
import ru.eltex.lab7.DeleteException;

import java.io.*;
import java.util.PriorityQueue;
import java.util.UUID;

public class ManagerOrderJSON extends AManageOrder {
    @Override
    public Order readById(UUID id, String fileName) {

        try (FileReader fr = new FileReader(fileName);
             BufferedReader br = new BufferedReader(fr))
        {
            ObjectMapper mapper = new ObjectMapper();
            String data;
            while ((data = br.readLine()) != null)
            {
                Order order = mapper.readValue(data, Order.class);
                if (order.uuid.equals(id))
                {
                    return order;
                }
            }
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void saveById(UUID id, Orders orders, String fileName) {
        Order order = orders.getOrderById(id);
        if (order != null)
        {
            try (FileWriter fw = new FileWriter(fileName);
                 BufferedWriter bw = new BufferedWriter(fw))
            {
                ObjectMapper mapper = new ObjectMapper();
                bw.write(mapper.writeValueAsString(order));
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public PriorityQueue<Order> readAll(String fileName) throws DeleteException
    {
        Orders orders = new Orders();
        try (FileReader fr = new FileReader(fileName);
             BufferedReader br = new BufferedReader(fr))
        {
            ObjectMapper mapper = new ObjectMapper();
            String data;
            while ((data = br.readLine()) != null)
            {
                orders.orders.add(mapper.readValue(data, Order.class));
            }
            return orders.orders;
        }catch (EOFException e) {
            if (orders.orders.size() > 0)
            {
                return orders.orders;
            }
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            throw new DeleteException(e.getMessage(), 2);
        }
        return null;
    }

    @Override
    public void saveAll(Orders orders, String fileName) {
        try (FileWriter fw = new FileWriter(fileName);
             BufferedWriter bw = new BufferedWriter(fw))
        {
            ObjectMapper mapper = new ObjectMapper();
            StringBuilder sb = new StringBuilder();
            orders.orders.forEach(o -> {
                try {
                    sb.append(mapper.writeValueAsString((Order)o) + "\n");
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
            });
            bw.write(sb.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
