package ru.eltex.lab5;

import ru.eltex.lab2.Order;
import ru.eltex.lab2.Orders;

import java.io.*;
import java.util.PriorityQueue;
import java.util.UUID;

public class ManagerOrderFile extends AManageOrder {
    @Override
    public Order readById(UUID id, String fileName) {
        Order order;
        try (FileInputStream is = new FileInputStream(fileName);
             ObjectInputStream ois = new ObjectInputStream(is))
        {
            while (true)
            {
                order = (Order)ois.readObject();
                if (order.uuid.equals(id))
                {
                    return order;
                }
            }
        }
        catch (EOFException e) {
            return null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void saveById(UUID id, Orders orders, String fileName) {
        Order order = orders.getOrderById(id);
        if (order != null)
        {
            try (FileOutputStream os = new FileOutputStream(fileName);
                 ObjectOutputStream oos = new ObjectOutputStream(os))
            {
                oos.writeObject(order);
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public PriorityQueue<Order> readAll(String fileName) {
        Orders orders = new Orders();
        try (FileInputStream is = new FileInputStream(fileName);
             ObjectInputStream ois = new ObjectInputStream(is))
        {
            Order order;
            while (true)
            {
                order = (Order)ois.readObject();
                orders.orders.add(order);
            }
        } catch (EOFException e) {
            if (orders.orders.size() > 0)
            {
                return orders.orders;
            }
        }catch (FileNotFoundException e) {
            return null;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void saveAll(Orders orders, String fileName) {
        try (FileOutputStream os = new FileOutputStream(fileName);
             ObjectOutputStream oos = new ObjectOutputStream(os))
        {
            orders.orders.forEach(o -> {
                try {
                    oos.writeObject(o);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
