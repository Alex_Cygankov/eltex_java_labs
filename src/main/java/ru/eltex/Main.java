package ru.eltex;

import ru.eltex.lab1.Coffee;
import ru.eltex.lab1.Drink;
import ru.eltex.lab1.DrinkType;
import ru.eltex.lab1.Tea;
import ru.eltex.helpers.DataGeneratorHelper;
import ru.eltex.lab2.Order;
import ru.eltex.lab2.Orders;
import ru.eltex.lab2.ShoppingCart;
import ru.eltex.lab6.Client;
import ru.eltex.lab6.Host;
import ru.eltex.lab4.CheckerFinished;
import ru.eltex.lab4.CheckerWaitings;
import ru.eltex.lab4.OrderGenerator;
import ru.eltex.lab5.ManagerOrderFile;
import ru.eltex.lab5.ManagerOrderJSON;
import ru.eltex.lab7.DeleteException;

import java.net.*;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.concurrent.TimeUnit;


public class Main {

    public static void main(String[] args) {
        byte b = -1;

        System.out.println(b);
        Main main = new Main();

        //main.exeq_lab#(args);
        //main.exeq_lab2(args);
        //main.exeq_lab4(args);
        main.exeq_lab5(args);
        //lab6
//        if (args.length == 1) {
//            if (args[0].equalsIgnoreCase("server")) {
//                DataGeneratorHelper dataGeneratorHelper = new DataGeneratorHelper();
//                Orders orders = dataGeneratorHelper.generateOrders();
//                main.exec_lab6_host(args, orders);
//            }
//        }
//        if (args.length >= 1){
//            if ( args[0].equalsIgnoreCase("client") )
//            {
//                DataGeneratorHelper dataGeneratorHelper = new DataGeneratorHelper();
//                main.exec_lab6_client(args);
//            }
//        }
    }

    private void exeq_lab1(String[] args) {
        if (args.length != 2) {
            System.out.println("Wrong arguments!");
        } else {
            int objectsCount = Integer.parseInt(args[0]);
            DrinkType objectsType = null;

            try {
                objectsType = DrinkType.values()[Integer.parseInt(args[1])];
            } catch (Exception e) {
                try {
                    objectsType = DrinkType.valueOf(args[1]);
                } catch (Exception ex) {

                }
            }
            if (objectsType == null) {
                System.out.println("Wrong objectsType!");
            } else {
                ArrayList<Coffee> coffeeList = new ArrayList<Coffee>();
                ArrayList<Tea> teaList = new ArrayList<Tea>();

                for (int i = 0; i < objectsCount; i++) {
                    switch (objectsType) {
                        case TEA: {
                            teaList.add(new Tea());
                            break;
                        }
                        case COFFEE: {
                            coffeeList.add(new Coffee());
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                }

                coffeeList.forEach(x -> x.create());
                teaList.forEach(x -> x.create());

                coffeeList.forEach(coffee -> coffee.read());
                teaList.forEach(tea -> tea.read());
                System.out.println("Count:\t" + Drink.getCount());
            }
        }
    }

    private void exeq_lab2(String[] args) {
        DataGeneratorHelper dataGeneratorHelper = new DataGeneratorHelper();
        Orders orders = dataGeneratorHelper.generateOrders();
        orders.showAll();
        while (orders.orders.size() > 0) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int finished = orders.checkOrders();
            if (finished > 0) {
                PriorityQueue<Order> removed = null;
                ShoppingCart<Drink> justCart = null;
                System.out.println("orders finished." + finished);
                removed = orders.removeFinished();
                System.out.println(removed.size() + " orders removed.");
                System.out.println(orders.orders.size() + " orders left.");

                justCart = removed.peek().cart;
                if (justCart.size() > 0) {
                    System.out.println("justCart content:");
                    System.out.println(justCart.showAll());
                    System.out.println("deleted record:");
                    Drink deleted = justCart.delete(0);
                    deleted.read();
                    System.out.println("result of search for deleted record:");
                    System.out.println(justCart.find(deleted.objectId) + "\n");
                } else {
                    System.out.println("justCart is empty.\n");
                }
            }
        }
    }

    private void exeq_lab4(String[] args) {
        DataGeneratorHelper dataHelper = new DataGeneratorHelper();
        Orders<Order> orders = new Orders<>();
        OrderGenerator oGen = new OrderGenerator(200, orders);
        CheckerWaitings checkerW = new CheckerWaitings(1000, orders);
        CheckerFinished checkerF = new CheckerFinished(5000, orders);
        Thread tGen = new Thread(oGen);
        Thread tCheckerW = new Thread(checkerW);
        Thread tCheckerF = new Thread(checkerF);
        tGen.start();
        tCheckerW.start();
        tCheckerF.start();
        try {
            Thread.sleep(100000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        oGen.runable = false;
        checkerW.runable = false;
        checkerF.runable = false;

    }

    private void exeq_lab5(String[] args) {
        DataGeneratorHelper dataGeneratorHelper = new DataGeneratorHelper();
        Orders orders = dataGeneratorHelper.generateOrders();
        orders.showAll();
        ManagerOrderFile binSaveLoad = new ManagerOrderFile();
        ManagerOrderJSON JSONSaveLoad = new ManagerOrderJSON();

        binSaveLoad.saveAll(orders, "savedAll.bin");
        binSaveLoad.saveById(((Order) orders.orders.toArray()[0]).uuid, orders, "savedById.bin");

        JSONSaveLoad.saveAll(orders, "savedAll.json");
        JSONSaveLoad.saveById(((Order) orders.orders.toArray()[0]).uuid, orders, "savedById.json");

        Orders readedBinAll = new Orders();
        readedBinAll.orders.addAll(binSaveLoad.readAll("savedAll.bin"));

        Order readedBinOne = binSaveLoad.readById(((Order) orders.orders.toArray()[0]).uuid, "savedAll.bin");

        Orders readedJSONAll = new Orders();
        try
        {
            readedJSONAll.orders.addAll(JSONSaveLoad.readAll("savedAll.json"));
        } catch (DeleteException e)
        {
            e.printStackTrace();
        }

        Order readedJSONOne = JSONSaveLoad.readById(((Order) orders.orders.toArray()[0]).uuid, "savedAll.json");

        System.out.println("Original \tBin \tJSON");
        System.out.println(orders.orders.size() + "\t" + readedBinAll.orders.size() + "\t" + readedJSONAll.orders.size());
        System.out.println(((Order) orders.orders.toArray()[0]).createTime + "\t" + readedBinOne.createTime + "\t" + readedJSONOne.createTime);
        System.out.println("Finished");
    }

    private void exec_lab6_client(String[] args) {
        Client client = null;
        switch (args.length)
        {
            case 3:
            {
                try {
                    client = new Client(Integer.parseInt(args[1]), InetAddress.getByName(args[2]));
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                break;
            }
            default:
            {
                try {
                    client = new Client();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        if (client != null) {
            client.getPortTCP();
            client.sendOrderTCP();
            client.listenUDP();
        }
    }

    private void exec_lab6_host(String[] args, Orders orders) {
        Host host = new Host(45000, orders);
        host.sendPortTCP();
        host.listenTCP();
    }
}
